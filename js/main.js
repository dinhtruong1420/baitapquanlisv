// CRUD ~ Create Read Update Delete

const DSSV = "DSSV";
var dssv = [];
// lấy dữ liệu lên từ localStorage
var dataJson = localStorage.getItem(DSSV);
if (dataJson) {
  //truthy falsy
  var dataRaw = JSON.parse(dataJson);
  // convert data từ local ( object lấy từ local sẽ mất method nên method tinhDTB ko còn => gây lỗi)

  //  Cách 1:
  // var result = [];
  // for (var index = 0; index < dataRaw.length; index++) {
  //   var currentData = dataRaw[index];
  //   var sv = new SinhVien(
  //     currentData.ma,
  //     currentData.ten,
  //     currentData.email,
  //     currentData.matKhau,
  //     currentData.diemLy,
  //     currentData.diemToan,
  //     currentData.diemHoa
  //   );
  //   result.push(sv);
  // }
  // dssv = result;

  // Cách 2:
  dssv = dataRaw.map(function (item) {
    return new SinhVien(
      item.ma,
      item.ten,
      item.email,
      item.matKhau,
      item.diemLy,
      item.diemToan,
      item.diemHoa
    );
  });

  renderDssv(dssv);
}
function saveLocalStorage() {
  // lưu xuống local
  var dssvJson = JSON.stringify(dssv);
  localStorage.setItem(DSSV, dssvJson);
}

function themSv() {
  var newSv = layThongTinTuForm();
  console.log("newSv: ", newSv);
  var isValid = true;

  // isValid =
  //   kiemTraRong(newSv.ma, "spanMaSV") &
  //   kiemTraRong(newSv.ten, "spanTenSV") &
  //   kiemTraRong(newSv.email, "spanEmailSV") &
  //   kiemTraRong(newSv.matKhau, "spanMatKhau") &
  //   kiemTraRong(newSv.diemToan, "spanToan");
  // kiemTraRong(newSv.matKhau, "spanMatKhau") &
  //   kiemTraRong(newSv.diemToan, "spanToan");
  // kiemTraRong(newSv.diemHoa, "spanHoa") & kiemTraRong(newSv.diemLy, "spanLy");

  // isValid = isValid && kiemTraMaSv(newSv.ma, dssv, "spanMaSV");

  // kiem tra ma sv
  isValid =
    isValid & kiemTraRong(newSv.ma, "spanMaSV") &&
    kiemTraMaSv(newSv.ma, dssv, "spanMaSV");

  //kiem tra ten
  isValid = isValid & kiemTraRong(newSv.ten, "spanTenSV");
  // kiem tra email

  isValid &=
    kiemTraRong(newSv.email, "spanEmailSV") &&
    kiemTraEmail(newSv.email, "spanEmailSV");
  // regex

  // if (!isValid) {
  //   return;
  // }
  // if (!isValid) retur;
  // isValid && dssv.push(newSv);
  if (isValid) {
    dssv.push(newSv);
    saveLocalStorage();
    renderDssv(dssv);
    resetForm();
  }
}

function xoaSv(idSv) {
  // indexOf ~ phần tử là string, number
  // findIndex ~ xử lý phần tử là object
  // tìm vị trí
  var index = dssv.findIndex(function (sv) {
    return sv.ma == idSv;
  });
  if (index == -1) {
    return;
  }
  //  xoá phần tử khỏi danh sách
  dssv.splice(index, 1);

  //  sau khi xoá thì dữ liệu thay đổi , nhưng layout ko có thay đổi vì layout được tạo ra nhờ renderDssv
  renderDssv(dssv);

  // update dữ liệu cho localStorage

  // saveLocalStorage();
}

function suaSv(idSV) {
  // findIndex nếu ko tìm thấy thì index = -1
  var index = dssv.findIndex(function (sv) {
    return sv.ma == idSV;
  });
  if (index == -1) return;

  var sv = dssv[index];

  // show thông tin lên layout
  showThongTinLenForm(sv);

  // disable input show mã sv
  document.getElementById("txtMaSV").disabled = true;
}

function capNhatSv() {
  var svEdit = layThongTinTuForm();

  var index = dssv.findIndex(function (sv) {
    return sv.ma == svEdit.ma;
  });
  if (index == -1) return;

  dssv[index] = svEdit;
  saveLocalStorage();
  renderDssv(dssv);
  resetForm();
  document.getElementById("txtMaSV").disabled = false;
}
// window.xoaSv = xoaSv;

// localStorage : chỉ lưu dc json

// json ~ convert data thành json

//6 giá trị falsy: null,  undefined,false, "", NaN, 0

// []

// if (null) {
//   console.log("Run");
//   //1
// }
// // 0

// module

// 122;
// ("alice");
// username;
// "XXDA"
// ("username");

var array1 = [1, 3, 5, 7];
console.log("array1: ", array1);

var array2 = array1.map(function (number) {
  console.log("number: ", number);
  return number * 2;
});
console.log("array2: ", array2);
